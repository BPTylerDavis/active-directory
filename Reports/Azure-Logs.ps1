# Import the needed Modules
Import-Module AzureAdPreview -RequiredVersion 2.0.2.149
Import-Module AzureAd
Import-Module MSOnline

# Open Connections to AzurePreview & MSOL
Connect-AzureAD
Connect-MsolService

#### Build Variables #####
$Datecreated = (Get-Date).AddDays(-10).ToString("MM/dd/yyyy") #msol
#$lastlogin2 = (Get-Date).AddDays(-5).ToString("yyyy-MM-dd") #azureadaudit
$lastlogin2 = (Get-Date).AddDays(-8).ToString("yyyy-MM-dd") #azureadaudit
$searchmonths = (Get-Date).AddMonths(-6).ToString("yyyy-MM-dd") #Looks at logs 6 months back

#Output Variables
$filedate = (Get-Date).ToString("yyyy-MM-dd") #Date for the file
$fileName = $filedate + "_Office365_User_Audit.csv" #String for csv name
$MFAFileName = $filedate + "_Office365_MFAUser_Audit.csv" #String for csv name
$excludeList = Get-Content "C:\Users\davist\Documents\GitHub\BP\AzureAudit_Exclude.txt" #Path to txt file with list of UserPrincipalNames to exclude from results (such as service Avccounts); One UPN per line.

#Populates the list of users we are going to check
$users_msol = Get-MsolUser -EnabledFilter EnabledOnly -All | Where-Object {$_.Whencreated -lt $Datecreated -and $_.isLicensed -eq $true -and $_.UserPrincipalName -like "*@broad-path.com"}
$mfausers = get-msoluser -All

#Fetches the Azure Sign In Logs to compare the users against

#$AzureAudit = Get-AzureADAuditSignInLogs -filter "createdDateTime gt $lastlogin' and activityDateTime gt $searchmonths" -All $true
#$filter = "createdDateTime gt '{0}' and activityDateTime gt '{1}'" -f $lastlogin2,$searchmonths;
#$AzureAudit = Get-AzureADAuditSignInLogs -All $true -filter "$filter"
$AzureAudit = Get-AzureADAuditSignInLogs -filter "createdDateTime gt $lastlogin2" -All $true

#Filters the returned logs for applications we are looking for, this step could probably be skipped makes the foreach loop slightly quicker.
$AzureAudit_filtered = $AzureAudit | select-object UserDisplayName, UserPrincipalName, CreatedDateTime, AppDisplayName |`
 Where-Object {$_.AppDisplayName -eq 'Windows Sign In' -or $_.AppDisplayName -eq 'Office 365 Exchange Online' -or $_.AppDisplayName -eq 'Microsoft Teams Web Client' -or $_.AppDisplayName -eq 'Microsoft Teams'`
  -or $_.AppDisplayName -eq 'Microsoft Office 365 Portal' -or $_.AppDisplayName -eq 'O365 Suite UX' -or $_.AppDisplayName -eq 'Office 365 SharePoint Online' -or $_.AppDisplayName -eq 'Skype Web Experience On Office 365' -or $_.AppDisplayName -eq 'Microsoft App Access Panel'`
  -or $_.AppDisplayName -eq 'Office.com' -or $_.AppDisplayName -eq 'SharePoint Online Web Client Extensibility'}

$staleUser = @() #Initialize an Array to store the output

foreach ($user in $users_msol)
    {
        if ($excludeList -contains $user.UserPrincipalName){write-host "Matches Excludelist"}
        else 
        {
            #We write the output to Write-host so we can capture the working information in a transcript of the script.
            if ($user.UserPrincipalName -in $AzureAudit_filtered.UserPrincipalName ) {write-host ("User {0} has logged in the last 5 days"-f $user.UserPrincipalName) } 
        
            #If we fail the first step, we mark the account as stale, then check it against the exlude list before writing it to the output array.
            Else {write-host ("{0} Has not logged in the last 5 days" -f $user.UserPrincipalName); $staleuser += $user}
        }   
    }

#$staleUser | Select-Object FirstName, LastName, DisplayName, UserPrincipalName, isLicensed, BlockCredential, Licenses.accountskuid, Department | Export-Csv c:\temp\bp\$fileName -NoTypeInformation

#Runs the query for the MFA Report
$mfa_status = $mfausers | Select-Object DisplayName,UserPrincipalName,Department, isLicensed, @{n="Disabled";e={$_.BlockCredential}},@{N="MFA Status"; E={ if( $_.StrongAuthenticationRequirements.State -ne $null){ $_.StrongAuthenticationRequirements.State} else { "Disabled"}}}, @{n="DirSynced"; E={if($_.LastDirSyncTime -ne $null){"True"}Else{"FALSE"}}}

# Collects the User Data and outputs it to a CSV; Usses an in-line array to pull the contents of the license for each user, as its a nested array.
$staleUser | Select-Object FirstName, LastName, DisplayName, UserPrincipalName, isLicensed, @{n="Disabled";e={$_.BlockCredential}}, @{n="Licenses Type";e={$_.Licenses.AccountSKUid}}, Department | Export-Csv c:\temp\bp\$fileName -NoTypeInformation

# Exports the MFA Report
$mfa_status | Export-Csv c:\temp\bp\$MFAFileName -NoTypeInformation


# $user = Get-AzureADAuditSignInLogs -Filter "userPrincipalName eq 'tikeitha.andrews@broad-path.com'" | Select AppDisplayName
#
# False Positives:
# tracy.porterfield@broad-path.com (interactive:DUO; Non Interactive)
# aubrey.just@broad-path.com (Non-Interactive)
#
#
# Get Employee ID and use it for report.
#
#
#
#
#
