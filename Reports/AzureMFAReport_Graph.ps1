# Import Modules
Import-Module AzureAdPreview -RequiredVersion 2.0.2.117 -usewindowspowershell;
Import-Module AzureAd -usewindowspowershell;
Select-MgProfile -Name Beta; Import-Module Microsoft.graph; # install-module Microsoft.Graph

# Connect to Services
Connect-AzureAD
Connect-MsolService
connect-graph -Scopes @("UserAuthenticationMethod.Read.All";"UserAuthenticationMethod.ReadWrite.All","User.Read.All","Group.ReadWrite.All")