Import-Module AzureAdPreview -RequiredVersion 2.0.2.117
Import-Module AzureAd

# Open Connection to MSOL
Connect-MsolService


# Define your list of users to update state in bulk
[array]$userlist = Get-Content "C:\Users\davist\Documents\GitHub\BP\Enforce_MFA.csv" #Path to csv file with list of UserPrincipalNames; One UPN per line.
$Export = @()  #Create Output Array

foreach ($u in $userlist){
    $Export += Get-MsolUser -UserPrincipalName $u | Select-Object UserPrincipalName,StrongAuthenticationMethods,StrongAuthenticationRequirements -ExpandProperty StrongauthenticationMethods | select UserPrincipalName,ISdefault,MethodType

}

# $Export += Get-MsolUser -UserPrincipalName Keonia.J.Rogers@broad-path.com | Select-Object UserPrincipalName,StrongAuthenticationMethods,StrongAuthenticationRequirements -ExpandProperty StrongauthenticationMethods | select UserPrincipalName,ISdefault,MethodType