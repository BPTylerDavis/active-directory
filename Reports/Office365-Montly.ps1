# Import the needed Modules
Import-Module AzureAdPreview -RequiredVersion 2.0.2.117
Import-Module AzureAd

# Open Connections to AzurePreview & MSOL
Connect-AzureAD
Connect-MsolService

$users_report = Get-MsolUser -EnabledFilter EnabledOnly -All | Where-Object {$_.isLicensed -eq $true}
$export = $users_report | select DisplayName,UsageLocation,@{n="Licenses Type";e={$_.Licenses.AccountSKUid}},SignInName,UserPrincipalName,@{n="Proxy Address";e={$_.proxyaddresses}}
$export | Export-Csv c:\temp\bp\UsersExport_6.30.2022.csv -NoTypeInformation