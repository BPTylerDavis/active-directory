Import-Module AzureAdPreview -RequiredVersion 2.0.2.117
Import-Module AzureAd

# Open Connections to AzurePreview & MSOL
Connect-AzureAD
Connect-MsolService

$Azurefiledate = (Get-Date).ToString("yyyy-MM-dd") #Date for the file
$AzurefileName = $azurefiledate + "AzureADUsers.csv" #String for csv name

$finalReport = @()

$azure_users = Get-AzureADUser -All $true
$filtered_users = $azure_users |  where {$_.AccountEnabled -eq "True"} | where {$_.UserPrincipalName -like "*@broad-path.com"} | where {$_.UserPrincipalName -notlike "bcbsaz*@broad-path.com"} | where {$_.UserPrincipalName -notlike "BPconf*@broad-path.com"} | where {$_.UserPrincipalName -notlike "bcbsaz*@broad-path.com"} |where  {$_.ObjectType -like "User"} |where {$_.UserPrincipalName -notlike "a-*@broad-path.com"} |where {$_.UserPrincipalName -notlike "package_*@broad-path.com"} | where {$_.UserPrincipalName -notlike "reporting-*@broad-path.com"} | where {$_.UserPrincipalName -notlike "bhive*@broad-path.com"} | where {$_.UserPrincipalName -notlike "bp*@broad-path.com"}

$filtered_users | foreach-object {
    $user = $_
    #Expanding only the Extension Attributes related to the user and converting the Dictionary to Custom Object so that keys can be accessed through the dot (.) operator
    $Extension_Attributes = New-Object Psobject -Property $user.ExtensionProperty 
    

$users_properties = [PSCustomObject]@{
    "Email" = $user.userPrincipalName
    "First Name" = $user.GivenName
    "Last Name" = $user.surname
    "Department" = $user.department
    "Job Title" = $user.jobtitle
    "EmployeeID" = $Extension_Attributes.EmployeeID
    }
$finalReport += $users_properties
}


$finalReport | export-csv C:\temp\bp\$AzurefileName; 


$finalReport = @()