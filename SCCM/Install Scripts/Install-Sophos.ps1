$temp = "C:\BPInstall\"
$SophosInstalled = Test-Path -Path "C:\Program Files\Sophos"
$InstallerSource = "https://api-cloudstation-us-east-2.prod.hydra.sophos.com/api/download/b5ae86b6089c830ea1bcd4b09e71724f/SophosSetup.exe"
$destination = "$temp\SophosSetup.exe"

If ($SophosInstalled){
Write-Host "Sophos is already installed. "
Sleep 3
Exit
} Else {
Write-Host "Beginning the installation"

If (Test-Path -Path $temp -PathType Container){
Write-Host "$temp already exists" -ForegroundColor Red
} Else {
New-Item -Path $temp -ItemType directory
}

Invoke-WebRequest $source -OutFile $destination
$WebClient = New-Object System.Net.WebClient
$webclient.DownloadFile($InstallerSource, $destination)
}

Start-Process -FilePath "$temp\SophosSetup.exe" -ArgumentList "--quiet"
 

 Param(
                [Parameter(Mandatory=$True,Position=1)]
                [string]$Searchbase
                )