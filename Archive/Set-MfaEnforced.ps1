Import-Module AzureAdPreview -RequiredVersion 2.0.2.117
Import-Module AzureAd

# Open Connection to MSOL
Connect-MsolService


# Define your list of users to update state in bulk
[array]$userlist = Get-Content "C:\Users\davist\Documents\GitHub\BP\Enforce_MFA.csv" #Path to csv file with list of UserPrincipalNames; One UPN per line.
$Export = @()  #Create Output Array
$state = "Enforced" #Define MFA State

# Export Variables
$filedate = (Get-Date).ToString("yyyy-MM-dd") #Date for the file
$MFAFileName = $filedate + "_Office365_MFAUpdates.csv" #String for csv name


foreach ($u in $userlist)
{
    $st = New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
    $st.RelyingParty = "*"
    $st.State = $state
    $sta = @($st)
    Set-MsolUser -UserPrincipalName $u -StrongAuthenticationRequirements $sta
    $Export += Get-MsolUser -UserPrincipalName $u | select UserPrincipalName,StrongAuthenticationMethods,StrongAuthenticationRequirements -ExpandProperty StrongAuthenticationREquirements | select UserPrincipalName, RelyingParty, State
    #set-msoluser -UserPrincipalName $u -UsageLocation "US"; Set-MsolUserLicense -UserPrincipalName $u -AddLicenses "BROADPATH:EMS"
}

$Export

#$Export | Export-Csv c:\temp\bp\$MFAFileName -NoTypeInformation
#Get-MsolUser -UserPrincipalName Orlando.Odtujan.Jr@broad-path.com | Select-Object UserPrincipalName,StrongAuthenticationMethods,StrongAuthenticationRequirements -ExpandProperty StrongAuthenticationREquirements | Select-Object UserPrincipalName, RelyingParty, State
#Set-MsolUser -UserPrincipalName Orlando.Odtujan.Jr@broad-path.com -StrongAuthenticationRequirements $sta