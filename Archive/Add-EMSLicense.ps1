Import-Module AzureAdPreview -RequiredVersion 2.0.2.117

# Open Connection to MSOL
Connect-AzureAD

$licenseduser = Get-MsolUser -All | where {$_.isLicensed -eq $true}


#$BPusers = $userlist | ForEach-Object {$u -like "*@broad-path.com"; return $u}
foreach ($l in $licenseduser)
{
    write-host $l.UserprincipalName "is being worked on"
    set-msoluser -UserPrincipalName $l.UserprincipalName -UsageLocation "US"; 
    Set-MsolUserLicense -UserPrincipalName $l.UserprincipalName -AddLicenses "BROADPATH:EMS";
}

