Import-Module AzureAdPreview -RequiredVersion 2.0.2.117
Import-Module AzureAd

Connect-ExchangeOnline -UserPrincipalName a-tyler.davis@broad-path.com -ShowProgress $true
Connect-AzureAD
Connect-MsolService

#
$Result = @()

$Raw_users = Import-Csv C:\Users\davist\Documents\active-directory\Exchange\Elixir\ElixirUsers.csv |where "QB Term Date" -EQ "$Null"
$users = $Raw_users | select "Full Name 2","Emp Id" -Unique

# Fetches AD Data from their data
foreach ($u in $users)
{
    $user = $u."full Name 2".Replace(",","")
    $result += Get-AzureADUser -SearchString $user
}






foreach ($u in $users)  
    {
        if ($u.'Full Name 2' -in $Result.displayname)
        {}
        Else
        {Write-Host $u.'Full Name 2' "Was not found"}
    }
