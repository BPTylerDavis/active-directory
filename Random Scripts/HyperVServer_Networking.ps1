
#Define Data Interfaces:
$I_data_1 = "6"; $I_data_2 = "12"

#Define ClusterTraffic Interfaces
$I_ClusterTraffic_1 = "13"; $I_ClusterTraffic_2 = "9"

#Define OSMgmt Interfaces:
$I_osmgmt_1 = "5"; $I_osmgmt_2 = "8"




# Rename adapters in windows for easier viewing/sorting
Get-NetAdapter -InterfaceIndex $I_data_1 | Rename-NetAdapter -NewName "data_1"
Get-NetAdapter -InterfaceIndex $I_data_2 | Rename-NetAdapter -NewName "data_2"

#Create LBFO Team with LACP
(New-NetLbfoTeam -name Data -TeamingMode Lacp -TeamMembers "{0}","{1}" -f $Data_1,$Data_2)

# Set IP Address (no IP on Data, as its Trunk for Hyperv guests)
#Get-NetAdapter -name "Data" | New-NetIPAddress -IPAddress -PrefixLength 24 -DefaultGateway


Get-NetAdapter -InterfaceIndex $I_ClusterTraffic_1 | Rename-NetAdapter -NewName "ClusterTraffic_1"
Get-NetAdapter -InterfaceIndex $I_ClusterTraffic_2 | Rename-NetAdapter -NewName "ClusterTraffic_2"

#Create LBFO Team with LACP
(New-NetLbfoTeam -name ClusterTraffic -TeamingMode Lacp -TeamMembers "{0}","{1}" -f $ClusterTraffic_1,$ClusterTraffic_2)

# Set IP Address
Get-NetAdapter -name "ClusterTraffic" | New-NetIPAddress -IPAddress 10.21.20.241 -PrefixLength 24 -DefaultGateway 10.21.20.1 -AddressFamily IPv4


#Define OSMgmt Interfaces:
$I_osmgmt_1 = "5"; $I_osmgmt_2 = "8"

Get-NetAdapter -InterfaceIndex $I_osmgmt_1 | Rename-NetAdapter -NewName "OSMgmg_1"
Get-NetAdapter -InterfaceIndex $I_osmgmt_2 | Rename-NetAdapter -NewName "OSMgmg_2"

#Create LBFO Team with LACP
(New-NetLbfoTeam -name OSMgmt -TeamingMode Lacp -TeamMembers "{0}","{1}" -f $osmgmt_1,$osmgmt_2)

# Set IP Address
Get-NetAdapter -name "OSMgmt" | New-NetIPAddress -IPAddress 10.21.210.241 -PrefixLength 24 -DefaultGateway 10.21.210.1 -AddressFamily IPv4
Get-NetAdapter -name "ClusterTraffic" | New-NetIPAddress -IPAddress 10.22.20.243 -PrefixLength 24 -DefaultGateway 10.22.20.1 -AddressFamily IPv4
Get-NetAdapter -name "DMZ" | New-NetIPAddress -IPAddress 10.22.90.243 -PrefixLength 24 -DefaultGateway 10.22.90.1 -AddressFamily IPv4

# Set DNS on Management Adapter
Get-NetAdapter -name "OSMgmt" | Set-DnsClientServerAddress -ServerAddresses ("10.11.1.5","8.8.8.8")