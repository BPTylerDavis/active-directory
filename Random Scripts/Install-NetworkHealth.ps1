cd C:\BPInstall\Community_Health\

#install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
Start-Sleep 5;

#Install 7zip powershell
#Install-Module -Name 7Zip4Powershell -Force

#Enable .net 4.5
enable-windowsoptionalfeature -featurename NetFx4 -Online
Start-Sleep 5;

#install application
#Start-Process '.\MiCC Client 9.1.3.0.exe'  -ArgumentList '/silent /workflow="Client Component Pack.deploy" /features="Ignite,ContactCenterClient,FlexibleReporting,ContactCenterSoftphone,MiVoiceBorderGatewayConnector,YourSiteExplorer,SalesforceConnector" /enterpriseip="10.245.13.25"' -Wait

start-sleep 15;
#Makeshift Health Rules Install
cp C:\BPInstall\Community_Health\HealthRules -Recurse 'C:\Program Files\' -Force
cp "C:\BPInstall\Community_Health\HealthEdge.Manager.lnk" 'C:\users\All Users\Desktop\'

