Import-Module ExchangeOnlineManagement
Connect-IPPSSession -UserPrincipalName a-tyler.davis@broad-path.com
$Search=New-ComplianceSearch -Name "PII Breach 6.17.2022" -ExchangeLocation All -ContentMatchQuery '(Received:6/08/2022..6/09/2022) AND (Subject:"Roster Alert: ESI Pharmacy Wave 7")'
Start-ComplianceSearch -Identity $Search.Identity
New-ComplianceSearchAction -SearchName "PII Breach 6.17.2022" -Purge -PurgeType HardDelete

Get-ComplianceSearchAction -Identity "PII Breach 6.17.2022_purge"