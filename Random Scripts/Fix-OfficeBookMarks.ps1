

# Script Global Variables
$users = Get-ChildItem C:\Users
$changeme = "https://login.microsoftonline.com/common/oauth2/authorize?client_id=00000006-0000-0ff1-ce00-000000000000&response_mode=form_post&response_type=code+id_token&scope=openid+profile&state=OpenIdConnect.AuthenticationProperties%3dfK3oqS4k3xRmOPMW-0N6YmaUr6sInjeJH-BHyL1p1DwcIu1BLtOFRo4fvKlvpQ-p-lwwyIrvCDa1Ml4forM6_zvbBRTACXegcJvO14Z85yTCKNvaR7qKXtie2A-UobledYlkO5OhuZl2gXltDgl6FtoKOYIoNYO0uRspN3ZrwKM&nonce=636697198584648419.YThkYjU3OWUtZTIxYS00YmY1LWE1OGUtZjI4NTA1YzY2ZGUxOWM0NDM0MDMtYWYwNS00MWU1LWI2OGUtMzA2OWZjZTc2YTA4&redirect_uri=https%3a%2f%2fportal.office.com%2flanding&ui_locales=en-US&mkt=en-US&client-request-id=a5287fc9-3ba6-4d1e-8d4f-7b35d9de7479"
$changeto = "portal.office.com"



foreach ($u in $users) 
{

# Dynamic path based in the foreach loop
$path = "C:\Users\$u\AppData\Local\Google\Chrome\User Data\Default\Bookmarks"

# testing vars, since i kinda like my bookmarks
 #$temp =  "c:\temp\Bookmarks";  $temp2 = "C:\temp\Bookmarks_new2"

 if (test-path $path) 
    {  
        # We found a hit, so we update the URL
        $content = [System.IO.File]::ReadAllText($path).Replace($changeme,$changeto)
        [System.IO.File]::WriteAllText($path, $content)
    } 
    Else 
    {
        # We didnt find a bookmarks file, so we break from proccesing
        break
    }
}
