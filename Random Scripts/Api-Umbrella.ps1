$encoded = 'MWNjNDFkMTViMGJhNDhiMTlkYjFhNjZkMDIwYThmODc6NjlkZDgxYTdjMWFlNDBkYWExZjU3MjNjMDQ3ZjI1MGM='
$companyid = '1910715'
$baseurl = 'https://management.api.umbrella.com/v1/organizations/'
$limiter = "limit=100"

$header = @{
    "Authorization:" = "Basic $encoded"
}
$formatedurl = $baseurl + "organizations/" + $companyid + "/roamingcomputers"
$url = "https://management.api.umbrella.com/v1/organizations/$companyid/roamingcomputers"

# curl -i GET https://management.api.umbrella.com/v1/organizations \
# -H 'Authorization: Basic %YourEncodedKeySecret%' \
# -H 'Content-Type: application/json'

$request = Invoke-RestMethod -Uri $url -Headers $header
$response = $request.requests