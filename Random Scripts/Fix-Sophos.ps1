

# Capture the data in the access registry
$Result = get-acl Registry::HKEY_USERS\S-1-5-19\SOFTWARE\Microsoft\SystemCertificates | Select-Object -ExpandProperty access

# Validate the data is contained in the registry
if ($result.IdentityReference.value -contains "NT AUTHORITY\LOCAL SERVICE")

#if the data is found, break out of the if/then loop
{break}

Else
# we didnt find the registry key, so we need to fix it:
{ 
    New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS
    $acl = Get-Acl HKU:\S-1-5-19\SOFTWARE\Microsoft\SystemCertificates
    $inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [system.security.accesscontrol.PropagationFlags]"None"
    $rule = New-Object System.Security.AccessControl.RegistryAccessRule ("Local Service","FullControl",$inherit,$propagation,"Allow")
    $acl.SetAccessRule($rule)
    $acl |Set-Acl -Path HKU:\S-1-5-19\SOFTWARE\Microsoft\SystemCertificates
}
