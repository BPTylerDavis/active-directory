# Import Modules
Import-Module AzureAdPreview
Import-Module AzureAd;
$MaximumFunctionCount = 8192; $MaximumVariableCount = 8192; #larger memory allocation for all the graph modules.
Select-MgProfile -Name Beta; Import-Module Microsoft.graph; # install-module Microsoft.Graph

# Connect to Services
Connect-AzureAD
Connect-MsolService
connect-graph -Scopes @("UserAuthenticationMethod.Read.All";"UserAuthenticationMethod.ReadWrite.All")


# Store the data from NewUsers.csv in the $ADUsers variable
$ADUsers = Import-Csv C:\temp\New_AdUsers_SSPR.csv -Delimiter ","


# MFA Variables
$state = "Enforced" #Define MFA State
$st = New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
    $st.RelyingParty = "*"
    $st.State = $state
    $sta = @($st)

# Country Variables
$Country_US = "United States", "US", "USA"
$Country_PH = "PH", "Philipines", "PH"

foreach ($User in $ADUsers) 
{
    # Define Variables for Loop Use
    $username = $User.email
    $username_1 = $username.replace("-", ".").replace(" ", ".").Replace("'","").Replace("``",""); $email = $username_1.Replace("broad.path.com","broad-path.com")
    $email_personal = $user.emailpersonal
    $telephone_personal = $User.telephonepersonal
    $telephone_personal_mfa = $User.telephonepersonal.replace("(", "").replace(")", "").Replace(" ","").Replace("-","")

    write-host "working on user $email"
    if     ($country_US -contains $user.Country) {$telephone_personal_mfa_county = "+1" + " " + $telephone_personal_mfa}
    elseif ($country_PH -contains $user.Country) {$telephone_personal_mfa_county = "+63" + " " + $telephone_personal_mfa}
    #elseif () {}


    # Checks to see what license level the user is at, and adds them to the correct Office 365 group
        if($user.License -ilike "e1") {Set-MsolUser -UserPrincipalName $email -UsageLocation "US"; Set-MsolUserLicense -UserPrincipalName $email -AddLicenses "broadpath:STANDARDPACK";} 
    Elseif($user.License -ilike "e3") {Set-MsolUser -UserPrincipalName $email -UsageLocation "US"; Set-MsolUserLicense -UserPrincipalName $email -AddLicenses "broadpath:ENTERPRISEPACK";}
        
    # Checks and Procceses Phone licensing (This is Beta)
    #   if($user.License -eq "$true") {Set-MsolUserLicense -UserPrincipalName $email -AddLicenses -AddLicenses "broadpath:MCOPSTN1"; Set-MsolUserLicense -AddLicenses "broadpath:MCOEV"; write-host "The user account $email was added to MCOPSTN & MCOEV" -ForegroundColor Blue}
    
    
    # Update Recovery Email And Phone
    #Set-MsolUser -UserPrincipalName $email -AlternateEmailAddresses @("$email_personal") -MobilePhone "$telephone_personal"

    
    # Set MFA Enforced
    #Set-MsolUser -UserPrincipalName kaela.schoon@inbhive.com -StrongAuthenticationRequirements $sta;
    Set-MsolUser -UserPrincipalName $email -StrongAuthenticationRequirements $sta;


    # Set Authentication Phone
    #if ($telephone_personal -eq "$null") {} Else    {New-MgUserAuthenticationPhoneMethod -UserId a-sean.obrien@broad-path.com -phoneType "mobile" -phoneNumber $telephone_personal_mfa_county}
    #if ($email_personal     -eq "$null") {} Else    {New-MgUserAuthenticationEmailMethod -UserId a-sean.obrien@broad-path.com -EmailAddress $email_personal}
    if ($telephone_personal -eq "$null") {} Else    {New-MgUserAuthenticationPhoneMethod -UserId $email -phoneType "mobile" -phoneNumber $telephone_personal_mfa_county}
    if ($email_personal     -eq "$null") {} Else    {New-MgUserAuthenticationEmailMethod -UserId $email -EmailAddress $email_personal}
}