# Import active directory module for running AD cmdlets
Import-Module ActiveDirectory;

# Store the data from NewUsers.csv in the $ADUsers variable
$ADUsers = Import-Csv C:\temp\bp\New_AdUsers.csv -Delimiter ","

# Define UPN
$UPN = "thedavisfamily.us"
#$UPN = "broad-path.com"
#$UPN = "ad.broad-path.com"

# Loop through each row containing user details in the CSV file
foreach ($User in $ADUsers) {


    #Define the Hash list of attributes we may update on a user
        #         groupname = $user.groupname
        #         position_level = $user.positionlevel
    $username_1 = $user.email; $username_1_split = $username_1.split("@"); $username = $username_1_split[0]
    $path = $user.OU
    $hash = @{
        identity = $Username
        givenname = $User.firstname
        surname = $User.lastname
        displayname = $user.DisplayName
        initials = $User.initials
        email = $User.email
        streetaddress = $User.streetaddress
        city = $User.city
        postalcode = $User.zipcode
        state = $User.state
        title = $User.jobtitle
        company = $User.company
        department = $User.department
        employeeid = $user.EmployeeID
    }

    #For Any Keys that are null in the hash Remove them
    $keysToRemove = $hash.keys | Where-Object {!$hash[$_]}
    $keysToRemove | Foreach-Object {$hash.remove($_)}

    # Check to see if the user already exists in AD
    if (Get-ADUser -F { SamAccountName -eq $username }) {
        Write-Host $username "Is an existing AD User"

        Set-ADUser @hash
        
        #Proccess Role group changes
        Add-ADGroupMember -Identity $user.groupname -Members $username

        #Proccess OU Changes
        Get-Aduser $username | Move-ADObject -TargetPath $path
    }
    else {

        # User does not exist then proceed to create the new user account
        # Account will be created in the OU provided by the $OU variable read from the CSV file
        New-ADUser `
            -SamAccountName $_.username `
            -UserPrincipalName "$_.username@$UPN" `
            -Name "$_.firstname $_.lastname" `
            -GivenName $_.firstname `
            -Surname $_.lastname `
            -Initials $_.initials `
            -DisplayName $_.displayname `
            -Enabled $True `
            -Path $_.OU `
            -City $_.city `
            -PostalCode $_.zipcode `
            -Company $_.company `
            -State $_.state `
            -StreetAddress $_.streetaddress `
            -OfficePhone $_.telephone `
            -EmailAddress $_.email `
            -Title $_.jobtitle `
            -Department $_.department `
            -EmployeeID $_.employeeid `
            -AccountPassword (ConvertTo-secureString $hash.password -AsPlainText -Force) -ChangePasswordAtLogon $false `


        # If user is created, show message.
        #Write-Host "The user account $username is created." -ForegroundColor Cyan

        # Checks to see what license level the user is at, and adds them to the correct Office 365 group
        #    if($user.License -ilike "e1") {Add-ADGroupMember -Identity 'set_office365_e1' -Members $Username; Write-Host "The user account $username was added to E1." -ForegroundColor Red} 
        #Elseif ($user.License -ilike "e3"){Add-ADGroupMember -Identity 'set_office365_e3' -Members $Username; Write-Host "The user account $username was added to E3." -ForegroundColor Red} 
        #Elseif ($user.License -ilike "e5"){Add-ADGroupMember -Identity 'set_office365_e5' -Members $Username; Write-Host "The user account $username was added to E5." -ForegroundColor Red}

        #Add the user the EMS group
        #    Add-ADGroupMember -Identity "set_office365_ems" -Members $username; Write-Host "The user account $username was added to EMS." -ForegroundColor yellow;
        
        #Proccess Role group changes
         #   Add-ADGroupMember -Identity $groupname -Members $Username
        
    }
}