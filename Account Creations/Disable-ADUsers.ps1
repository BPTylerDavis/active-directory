Import-Module ActiveDirectory

#   Termination Paths
$OU = "OU=Terminations,OU=AccountCreations,OU=Test,OU=Domain Users,DC=thedavisfamily,DC=us"
$OU_Disabled = "OU=Disabled," + $OU
$OU_LOA = "OU=LOA," + $OU

#   Dates
$DaysToKeep = "30" #the ammount of days a user should remain disabled before being deleted
$script_date = Get-Date -format MM/dd/yyyy
$delete_date = (get-date).AddDays($DaysToKeep).ToString("MM/dd/yyyy")


[array]$userlist = Import-Csv "C:\Users\davist\Documents\GitHub\BP\ActiveDirectory\DisabledUsers.csv" #Path to csv file with list of UserPrincipalNames; One UPN per line.
$Export = @()  #Create Output Array

#   Start of Main Loop
foreach ($U in $userlist)
        {
            #Split the UPN to Fetch SamAccountName
            $stringsplit = $u.UserPrincipalName.Split("@")
            $u | Add-Member -NotePropertyName "Name" -NotePropertyValue $stringsplit[0]


            #Start proccesing a termination
            if ($u.action -eq "Termination") {
                                                Write-Host $u.UserprincipalName "Is a termination"
                                                Disable-ADAccount $u.Name;
                                                get-aduser $u.name | Move-ADObject -targetpath $OU_Disabled
                                                Set-ADUser -identity $u.name -description "Account Disabled on $script_date Pending Deletion on $delete_date"

                                                # Remove Licenses
                                                write-host "Removing licenses from the termination "$u.userprinipalname
                                                $userList = Get-AzureADUser -ObjectID $u.UserprincipalName
                                                $Skus = $userList | Select -ExpandProperty AssignedLicenses | Select SkuID
                                                if($userList.Count -ne 0) {
                                                    if($Skus -is [array])
                                                    {
                                                        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses
                                                        for ($i=0; $i -lt $Skus.Count; $i++) {
                                                            $Licenses.RemoveLicenses +=  (Get-AzureADSubscribedSku | Where-Object -Property SkuID -Value $Skus[$i].SkuId -EQ).SkuID   
                                                        }
                                                        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses
                                                    } else {
                                                        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses
                                                        $Licenses.RemoveLicenses =  (Get-AzureADSubscribedSku | Where-Object -Property SkuID -Value $Skus.SkuId -EQ).SkuID
                                                        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses
                                                    }
                                                }
                                            }
            

            
            #Start Proccesing a LOA
            elseif ($u.Action -eq "LOA") 
                                            {
            
                                                Write-Host $u.UserprincipalName "Is a LOA"
                                                Disable-ADAccount $u.Name;
                                                get-aduser $u.name | Move-ADObject -targetpath $OU_LOA
                                                Set-ADUser -identity $u.name -description "Account Disabled on $script_date, LOAStart=$u.LOAStart, LOAend=$u.LOA_End"
            
            
            
            
                                            }
            #Start Proccesing a LOA
            elseif ($u.Action -eq "drop") 
                                            {

                                                Write-Host $u.UserprincipalName "Is a Drop Deleting account and removing licenses"
                                                Remove-aduser $u.Name;

                                                # Remove Licenses
                                                write-host "Removing licenses from the termination "$u.userprinipalname
                                                $userList = Get-AzureADUser -ObjectID $u.UserprincipalName
                                                $Skus = $userList | Select -ExpandProperty AssignedLicenses | Select SkuID
                                                if($userList.Count -ne 0) {
                                                    if($Skus -is [array])
                                                    {
                                                        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses
                                                        for ($i=0; $i -lt $Skus.Count; $i++) {
                                                            $Licenses.RemoveLicenses +=  (Get-AzureADSubscribedSku | Where-Object -Property SkuID -Value $Skus[$i].SkuId -EQ).SkuID   
                                                        }
                                                        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses
                                                    } else 
                                                    {
                                                        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses
                                                        $Licenses.RemoveLicenses =  (Get-AzureADSubscribedSku | Where-Object -Property SkuID -Value $Skus.SkuId -EQ).SkuID
                                                        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses
                                                    }
                                                }
                                            }


                                            
        }
#   End of Main Loop


#   Cleanup Loop
