# Import active directory module for running AD cmdlets
Import-Module ActiveDirectory;
  
# Store the data from NewUsers.csv in the $ADUsers variable

$ADUsers = Import-Csv "C:\Scripts\Active Directory\New_AdUsers_SSPR.csv" -Delimiter "," # Update this with the path to your Excel File

# Commands for setting Self Service Password Reset#
# Set-MsolUser -UserPrincipalName user@domain.com -AlternateEmailAddresses @("email@domain.com") -MobilePhone "+1 4251234567"

#### The following code is only used for Rick and his broken laptop ####
## Define PS Drive for users not on a local AD Joined Computer
#New-PSDrive -Name HQ -PSProvider ActiveDirectory -Server ad.broad-path.com -Root "//RootDSE/" -Scope Global -Credential Get-Credential
#cd HQ:

# Define UPN
$UPN = "broad-path.com" #marks users for production/AzureAD
#$UPN = "ad.broad-path.com" #Marks users for local ad only

# Used for labNetwork
##$UPN = "thedavisfamily.us"

# Loop through each row containing user details in the CSV file
foreach ($User in $ADUsers) {

    #Read user data from each field in each row and assign the data to a variable as below  
    $password = $User.password
    $firstname = $User.firstname
    $lastname = $User.lastname.Replace(".", "")
    $username_1 = $firstname + "." + $lastname
    $username = $username_1.replace("-", ".").replace(" ", ".").Replace("'","").Replace("``","")
    $samaccountname_trimmed = $username -replace "(?<=.{20}).*" #trim sam account name to be 20 characters long
    
    #verify the trimmed name does not end in a period, as that invalidated new-aduser
    if($samaccountname_trimmed.EndsWith('.')){$samaccountname = $samaccountname_trimmed -replace ".$"}else{$samaccountname = $samaccountname_trimmed} 
    $displayname = $user.DisplayName
    $name = "$firstname $lastname"
    $initials = $User.initials
    $OU = $User.ou #This field refers to the OU the user account is to be created in
    $email = $User.email
    $streetaddress = $User.streetaddress
    $city = $User.city
    $zipcode = $User.zipcode
    $state = $User.state
    $telephone = $User.telephone
    $jobtitle = $User.jobtitle
    $company = $User.company
    $department = $User.department
    $employeeid = $user.EmployeeID
    #$username = $user.username                             # Uncomment line if username field exists
    #$username = $user.FirstName + "." + $user.LastName     # Uncomment line if username field doesent exist
    #if ($User.$username -ne $null) {$username = $user.username}else{$username = $user.FirstName + "." + $user.LastName } #possible automation, not tested. lazy admin.

    # Check to see if the username already exists in AD
    if (Get-ADuser -F { SamAccountName -eq $username }) {

        # If user already exists pull employee ID
        $GetADU = (Get-ADuser $username -properties employeeid)

        # If employee ID exists in AD then update OU Attributes using variables from the CSV file
        if ($GetADU.employeeid -eq $employeeid)
        {
            Write-Warning "A user account with username $username matching Employee ID $employeeid exists in Active Directory, updating user information."
            $GetADU | Move-ADObject -TargetPath $OU
            #Set-ADAccountPassword -Identity $username -Reset -NewPassword (ConvertTo-secureString $password -AsPlainText -Force)
            Set-ADUser $username `
                -Enabled $True `
                -City $city `
                -PostalCode $zipcode `
                -Company $company `
                -State $state `
                -StreetAddress $streetaddress `
                -OfficePhone $telephone `
                -EmailAddress $email `
                -Title $jobtitle `
                -Department $department
       
            # Checks to see what license level the user is at, and adds them to the correct Office 365 group
            if($user.License -ilike "e1") {Add-ADGroupMember -Identity 'set_office365_e1' -Members $samaccountname;} 
            Elseif ($user.License -ilike "e3"){Add-ADGroupMember -Identity 'set_office365_e3' -Members $samaccountname;} 
            Elseif ($user.License -ilike "e5"){Add-ADGroupMember -Identity 'set_office365_e5' -Members $samaccountname;}
            Elseif ($user.License -ilike "p1"){Add-AdGroupMember -Identity 'set_office365_p1' -Members $samaccountname;}
        }
        else {

            # If existing username with a non-matching employee ID exists in AD display warning
            Write-Warning "A user account with username $username already exists in Active Directory. - Different username needed"
        }
    }

    # User does not exist then proceed to create the new user account
    else {
        
        # Account will be created in the OU provided by the $OU variable read from the CSV file
        New-ADUser `
            -SamAccountName $samaccountname `
            -UserPrincipalName "$username@$UPN" `
            -Name "$name" `
            -GivenName $firstname `
            -Surname $lastname `
            -Initials $initials `
            -DisplayName $displayname `
            -Enabled $True `
            -Path $OU `
            -City $city `
            -PostalCode $zipcode `
            -Company $company `
            -State $state `
            -StreetAddress $streetaddress `
            -OfficePhone $telephone `
            -EmailAddress $email `
            -Title $jobtitle `
            -Department $department `
            -EmployeeID $employeeid `
            -AccountPassword (ConvertTo-secureString $password -AsPlainText -Force) -ChangePasswordAtLogon $false


        
        # Checks to see what license level the user is at, and adds them to the correct Office 365 group
            if ($user.License -ilike "e1"){Add-ADGroupMember -Identity 'set_office365_e1' -Members $samaccountname;} 
        Elseif ($user.License -ilike "e3"){Add-ADGroupMember -Identity 'set_office365_e3' -Members $samaccountname;} 
        Elseif ($user.License -ilike "e5"){Add-ADGroupMember -Identity 'set_office365_e5' -Members $samaccountname;}
        Elseif ($user.License -ilike "p1"){Add-AdGroupMember -Identity 'set_office365_p1' -Members $samaccountname;}
    }
}
Read-Host -Prompt "Press Enter to exit"